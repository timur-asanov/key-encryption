[![pipeline status](https://gitlab.com/timur-asanov/key-encryption/badges/master/pipeline.svg)](https://gitlab.com/timur-asanov/key-encryption/-/commits/master) [![coverage report](https://gitlab.com/timur-asanov/key-encryption/badges/master/coverage.svg)](https://gitlab.com/timur-asanov/key-encryption/-/commits/master)

### What is this utility?

This utility can be used to encrypt, and split into parts, short text or string keys.

For example, you want to encrypt text "test key" using the password "my password". This utility will encrypt that text to something like "auPFNAwiBAa5cITwzLqxMBobu1T3FpaExXgK7jCTpIg=". This encrypted text can now be hidden, and if it is found it will mean nothing without your password.

You can also split text into secret parts for sharing or distributing. Using the same text "test key", this utility can split this into 3 (or more) parts:

    1-65-9491028138606129061
    2-65-10594819453567531473
    3-65-11698610768528933885

These parts can be distributed among people or hidden in different places. To reconstruct the original text you will need at least 2 of these parts.

Encrypting and splitting text can be done together for more protection.

### Documentation

The documentation on how to use and install this utility can be found here: https://timur-asanov.gitlab.io/key-encryption

### Releases

v1.1
- added help messages
- updated docs
- changed build to include only the main docs HTML file, and not package CSS, JS, GIFs
- made the --help option standalone so that it doesn't get called with another option

v1.0
- Initial release.
