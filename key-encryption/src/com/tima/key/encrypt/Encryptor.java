package com.tima.key.encrypt;

public interface Encryptor {

	public final static String algorithm = "PBEWITHSHA256AND256BITAES-CBC-BC";
	
	public String encrypt(char[] password, String message);
	public String decrypt(char[] password, String message);
}
