package com.tima.key.encrypt;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 * An encryptor that uses the Jasypt library 
 * and Bouncy Castle as a provider.
 * 
 * @author Timur Asanov | tima@timurasanov.com
 */
public class JasyptBCEncryptor implements Encryptor {

	public JasyptBCEncryptor() {}
	
	private StandardPBEStringEncryptor setUp(char[] password) {
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setProvider(new BouncyCastleProvider());
		encryptor.setAlgorithm(algorithm);
		encryptor.setPasswordCharArray(password);
		
		return encryptor;
	}
	
	public String encrypt(char[] password, String message) {
		StandardPBEStringEncryptor encryptor = setUp(password);
		
		return encryptor.encrypt(message);
	}
	
	public String decrypt(char[] password, String message) {
		StandardPBEStringEncryptor encryptor = setUp(password);
		
		return encryptor.decrypt(message);
	}
}
