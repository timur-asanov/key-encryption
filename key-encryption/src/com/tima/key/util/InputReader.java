package com.tima.key.util;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tima.key.split.SecretPart;

public class InputReader {

	private static final Logger logger = LoggerFactory.getLogger(InputReader.class);

	/**
	 * Input for a key, either to encrypt it or decrypt it.
	 * @throws ValidationException 
	 */
	public static String inputKey(Scanner scanner) throws ValidationException {
		String key = null;

		System.out.print("Enter a key: ");
		key = scanner.nextLine().trim();

		logger.trace("entered key: " + key);
		
		Utilities.validateKey(key);

		return key;
	}

	/**
	 * Input for a password, either for encryption use or decryption use.
	 * @throws ValidationException 
	 */
	public static char[] inputPassword(Scanner scanner) throws ValidationException {
		char[] pwdArray = null;

		System.out.print("Enter a password: ");
		String pwd = scanner.nextLine().trim();

		pwdArray = pwd.toCharArray();

		logger.trace("entered password: " + new String(pwdArray));
		
		Utilities.validatePassword(pwdArray);

		return pwdArray;
	}

	/**
	 * Input for secret parts.
	 */
	public static SecretPart[] inputParts(Scanner scanner, int numKeys) throws ValidationException {
		SecretPart[] result = new SecretPart[numKeys];

		for (int i = 0; i < numKeys; i++) {
			System.out.print("Enter a secret part: ");
			String key = scanner.nextLine().trim();
			
			result[i] = Utilities.splitStringIntoPart(key);
		}

		return result;
	}
	
	/**
	 * Parses a command line parameter as an integer.
	 * 
	 * @param cliValue - the cli parameter
	 * 
	 * @return An integer value of the parameter.
	 * 
	 * @throws ValidationException - If the parameter cannot be converted to an integer.
	 */
	public static int readInt(String cliValue) throws ValidationException {
		int result = 0;
		
		try {
			if (Utilities.isEmpty(cliValue) == false) {
				result = Integer.parseInt(cliValue.trim());
			}
		} catch (Exception e) {
			throw new ValidationException("[" + cliValue + "] is not a valid number.");
		}
		
		return result;
	}
}
