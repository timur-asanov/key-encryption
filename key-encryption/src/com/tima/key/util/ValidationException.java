package com.tima.key.util;

public class ValidationException extends Exception {

	private static final long serialVersionUID = -1589559659767647186L;

	public ValidationException(String message) {
		super(message);
	}
}
