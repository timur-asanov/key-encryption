package com.tima.key.util;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tima.key.split.SSSSecretPart;
import com.tima.key.split.SecretPart;

public class Utilities {

	private static final Logger logger = LoggerFactory.getLogger(Utilities.class);

	public static final int KEYS_TO_RECONSTRUCT_MIN = 2;
	public static final int KEYS_MIN = 3;
	public static final int KEYS_MAX = 10;
	
	public static BigInteger generatePrime(int length) {
		BigInteger prime = new BigInteger("2").pow(length).subtract(new BigInteger("1"));
		
		logger.trace("prime bit length: " + prime.bitLength());
		
		return prime;
	}
	
	public static int product(List<Integer> integers) {
		int result = 1;
		
		for (int i : integers) {
			result *= i;
		}
		
		return result;
	}
	
	public static BigInteger sum(List<BigInteger> bigIntegers) {
		BigInteger result = BigInteger.ZERO;
		
		for (BigInteger i : bigIntegers) {
			result = result.add(i);
		}
		
		return result;
	}
	
	public static boolean isEmpty(String value) {
		if (value != null && value.trim().length() > 0) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Splits a secret part string into a secret part object.
	 */
	public static SecretPart splitStringIntoPart(String value) throws ValidationException {
		SecretPart part = null;
		
		if (isEmpty(value) == true) {
			throw new ValidationException("The secret part is empty.");
		}
		
		String[] splitValues = value.split(SSSSecretPart.PART_SEPARATOR);
		
		if (splitValues.length != SSSSecretPart.NUM_SUB_PARTS) {
			throw new ValidationException("The secret part is in the wrong format.");
		}
		
		try {
			part = new SSSSecretPart(Integer.valueOf(splitValues[0].trim()), Integer.valueOf(splitValues[1].trim()), new BigInteger(splitValues[2].trim()));
		} catch (Exception e) {
			throw new ValidationException("Was not able to parse the entered part [" + value + "].");
		}
		
		return part;
	}
	
	/**
	 * Validates a key.
	 */
	public static void validateKey(String key) throws ValidationException {
		// key cannot be empty
		if (isEmpty(key) == true) {
			throw new ValidationException("The key is invalid, empty.");
		}
	}
	
	/**
	 * Validates a password.
	 */
	public static void validatePassword(char[] pwdArray) throws ValidationException {
		// password cannot be empty
		if (pwdArray == null || pwdArray.length == 0 || new String(pwdArray).trim().length() == 0) {
			throw new ValidationException("The password is invalid, empty.");
		}
	}
	
	/**
	 * Validates inputs for the operation of splitting, or the operation of encrypting and splitting.
	 * 
	 * @param numKeys - number of keys required to reconstruct the key
	 * @param totalParts - total number of parts to split the key into
	 * 
	 * @throws ValidationException
	 */
	public static void validateForwards(int numKeys, int totalParts) throws ValidationException {
		validateNumKeys(numKeys);
		
		// total parts must be in the allowed range
		if (totalParts < KEYS_MIN) {
			throw new ValidationException("The number of total parts [" + numKeys + "] for secret sharing is too low.");
		} else if (totalParts > KEYS_MAX) {
			throw new ValidationException("The number of total parts [" + numKeys + "] for secret sharing is too high.");
		}
		
		// number of keys to reconstruct cannot be more than the total number of parts
		if (numKeys > totalParts) {
			throw new ValidationException("The number of keys [" + numKeys + "] to reconstruct the secret cannot be larger than the number of total parts [" + totalParts + "].");
		}
	}
	
	/**
	 * Validates inputs for the operation of combining, or the operation of decrypting and combining.
	 * 
	 * @param numKeys - number of keys required to reconstruct the key
	 * @param parts - parts that will be used fo reconstruction
	 * 
	 * @throws ValidationException
	 */
	public static void validateBackwards(int numKeys, SecretPart[] parts) throws ValidationException {
		validateNumKeys(numKeys);
		
		// reduce the parts to only distinct parts
		List<SecretPart> distinctParts = Arrays.stream(parts).distinct().collect(Collectors.toList());
		
		// the distinct parts should be all the parts that were entered
		if (distinctParts == null || distinctParts.size() != parts.length) {
			throw new ValidationException("The secret parts must be distinct.");
		}
		
		// the prime bit length must be the same on all parts
		int primeBitLenght = ((SSSSecretPart) parts[0]).getPrimeBitLength();
		
		for (int i = 1; i < parts.length; i++) {
			if (primeBitLenght != ((SSSSecretPart) parts[i]).getPrimeBitLength()) {
				throw new ValidationException("The secret parts must have been made during the same split.");
			}
		}
	}
	
	public static void validateNumKeys(int numKeys) throws ValidationException {
	
		// number of keys to reconstruct must be in the allowed range
		if (numKeys < KEYS_TO_RECONSTRUCT_MIN) {
			throw new ValidationException("The number of keys [" + numKeys + "] to reconstruct the secret is too low.");
		} else if (numKeys > KEYS_MAX) {
			throw new ValidationException("The number of keys [" + numKeys + "] to reconstruct the secret is too high.");
		}
	}
}
