package com.tima.key.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelperMessages {
	
	private static final Logger logger = LoggerFactory.getLogger(HelperMessages.class);

	public static void printHelpMessage() {
		logger.info("");
		logger.info("NAME");
		logger.info("\t" + "./ke.sh - A tool to encrypt / decrypt / secret share / secret combine for short text / keys.");
		logger.info("");
		logger.info("SYNOPSIS");
		logger.info("\t" + "./ke.sh [-e] [-ds | -k .. -p ..]");
		logger.info("\t" + "./ke.sh [-d] [-ws | -k ..]");
		logger.info("\t" + "./ke.sh [-s] [-k .. -p ..]");
		logger.info("\t" + "./ke.sh [-c] [-k ..]");
		logger.info("");
		logger.info("DESCRIPTION");
		logger.info("\t" + "This tool lets you encrypt and / or secret share a short string or a key such as a cryptocurrency \n\t"
							+ "wallet key. This will prevent anyone from stealing your information / money if they happen to find your \n\t"
							+ "key in plain text.");
		logger.info("");
		logger.info("\t" + "The major options of this tool are: ");
		logger.info("");
		logger.info("\t" + "-h, --help" + "\t" + "Specifying this option will print this message.");
		logger.info("");
		logger.info("\t" + "-v, --verbose" + "\t" + "If provided, the program will output more information while running.");
		logger.info("");
		logger.info("\t" + "-e, --encrypt" + "\t" + "If provided, the tool will start the encryption functionality. Based on additional options, shown \n\t\t\t"
													+ "below, this will allow you to encrypt a key with or without splitting that key into several parts \n\t\t\t"
													+ "after encryption.");
		logger.info("");
		logger.info("\t" + "-d, --decrypt" + "\t" + "If provided, the tool will start the decryption functionality. Based on additional options, shown \n\t\t\t"
													+ "below, this will allow you to decrypt a key by itself or first combine the key from parts and then \n\t\t\t"
													+ "decrypt it.");
		logger.info("");
		logger.info("\t" + "-s, --split" + "\t" + "If provided, the tool will start the split only functionality to split a key into parts.");
		logger.info("");
		logger.info("\t" + "-c, --combine" + "\t" + "If provided, the tool will start the combine only functionality to combine parts into a key.");
		logger.info("");
		
		printEncryptionHelpMessage();
		printDecryptionHelpMessage();
		printSplitHelpMessage();
		printCombineHelpMessage();
	}
	
	public static void printEncryptionHelpMessage() {
		logger.info("");
		logger.info("\t" + "Below are the valid options of the [-e] encryption functionality.");
		logger.info("");
		logger.info("\t" + "-ds, --dont_split" + "\t" + "If this option is provided during encryption, the entered key will be encrypted \n\t\t\t\t" 
														+ "but will not be split into parts. The [-k] and [-p] options will be ignored if  \n\t\t\t\t"
														+ "this is specified.");
		logger.info("");
		logger.info("\t" + "-k ..., --keys ..." + "\t" + "This option allows you to specify the number of parts that are required to reconstruct \n\t\t\t\t"
															+ "the key. The value must be between [" + Utilities.KEYS_TO_RECONSTRUCT_MIN + "] and [" + Utilities.KEYS_MAX + "]. "
															+ "It must also be lower than or equal to the total number of parts \n\t\t\t\t"
															+ "specified for option [-p]");
		logger.info("");
		logger.info("\t" + "-p ..., --parts ..." + "\t" + "This option allows you to specify the total number of parts to split the key into. \n\t\t\t\t"
															+ "The value must be between [" + Utilities.KEYS_MIN + "] and [" + Utilities.KEYS_MAX + "].");
		logger.info("");
	}
	
	public static void printDecryptionHelpMessage() {
		logger.info("");
		logger.info("\t" + "Below are the valid options of the [-d] decryption functionality.");
		logger.info("");
		logger.info("\t" + "-ws, --without_split" + "\t" + "If this option is provided during decryption, the tool will not combine any parts to form  \n\t\t\t\t"
															+ "a key, it will just decrypt the key. The [-k] option will be ignored if this is specified.");
		logger.info("");
		logger.info("\t" + "-k ..., --keys ..." + "\t" + "This option allows you to specify the number of parts that are required to reconstruct \n\t\t\t\t"
															+ "the key. The value must be between [" + Utilities.KEYS_TO_RECONSTRUCT_MIN + "] and [" + Utilities.KEYS_MAX + "].");
		logger.info("");
	}
	
	public static void printSplitHelpMessage() {
		logger.info("");
		logger.info("\t" + "Below are the valid options of the [-s] split functionality.");
		logger.info("");
		logger.info("\t" + "-k ..., --keys ..." + "\t" + "This option allows you to specify the number of parts that are required to reconstruct \n\t\t\t\t"
															+ "the key. The value must be between [" + Utilities.KEYS_TO_RECONSTRUCT_MIN + "] and [" + Utilities.KEYS_MAX + "]. "
															+ "It must also be lower than or equal to the total number of parts \n\t\t\t\t"
															+ "specified for option [-p]");
		logger.info("");
		logger.info("\t" + "-p ..., --parts ..." + "\t" + "This option allows you to specify the total number of parts to split the key into. \n\t\t\t\t"
															+ "The value must be between [" + Utilities.KEYS_MIN + "] and [" + Utilities.KEYS_MAX + "].");
		logger.info("");
	}
	
	public static void printCombineHelpMessage() {
		logger.info("");
		logger.info("\t" + "Below are the valid options of the [-c] combine functionality.");
		logger.info("");
		logger.info("\t" + "-k ..., --keys ..." + "\t" + "This option allows you to specify the number of parts that are required to reconstruct \n\t\t\t\t"
															+ "the key. The value must be between [" + Utilities.KEYS_TO_RECONSTRUCT_MIN + "] and [" + Utilities.KEYS_MAX + "].");
		logger.info("");
	}
}
