package com.tima.key.split;

import java.math.BigInteger;

public abstract class SecretPart {

	public abstract int getOrder();
	public abstract BigInteger getPart();
}
