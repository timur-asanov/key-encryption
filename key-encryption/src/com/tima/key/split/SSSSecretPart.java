package com.tima.key.split;

import java.math.BigInteger;

/**
 * Secret part representation for the Shamir's Secret Sharing algorithm.
 * 
 * @author Timur Asanov | tima@timurasanov.com
 */
public class SSSSecretPart extends SecretPart {
	
	public static final String PART_SEPARATOR = "-";
	public static final int NUM_SUB_PARTS = 3;

	private int order;
	private int primeBitLength;
	private BigInteger part;
	
	public SSSSecretPart() {}
	
	public SSSSecretPart(int order, int primeBitLength, BigInteger part) {
		this.order = order;
		this.part = part;
		this.primeBitLength = primeBitLength;
	}

	public int getOrder() {
		return order;
	}

	public int getPrimeBitLength() {
		return primeBitLength;
	}

	public BigInteger getPart() {
		return part;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + order;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SSSSecretPart other = (SSSSecretPart) obj;
		if (order != other.order)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return order + PART_SEPARATOR + primeBitLength + PART_SEPARATOR + part;
	}
}
