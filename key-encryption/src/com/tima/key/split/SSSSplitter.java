package com.tima.key.split;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import com.tima.key.util.Utilities;

/**
 * Implementation of Shamir's Secret Sharing algorithm.
 * 
 * @author Timur Asanov | tima@timurasanov.com
 */
public class SSSSplitter implements Splitter {

	public SSSSplitter() {}
	
	private BigInteger calculatePolynomial(BigInteger[] coefficients, int x, BigInteger prime) {
		BigInteger result = BigInteger.ZERO;
		
		for (int i = coefficients.length - 1; i >= 0; i--) {
			result = result.multiply(BigInteger.valueOf(x)).add(coefficients[i]).mod(prime);
		}
		
		return result;
	}
	
	public SecretPart[] split(BigInteger secret, int numKeys, int totalParts, BigInteger prime) {
		SSSSecretPart[] parts = new SSSSecretPart[totalParts];
		BigInteger[] coefficients = new BigInteger[numKeys];
		coefficients[0] = secret;
		
		for (int i = 1; i < numKeys; i++) {
			BigInteger randomNum;
			
			while (true) {
				randomNum = new BigInteger(prime.bitLength(), new SecureRandom());
				
				if (randomNum.compareTo(BigInteger.ZERO) > 0 && randomNum.compareTo(prime) <= 0) {
					break;
				}
			}
			
			coefficients[i] = randomNum;
		}
		
		for (int i = 1; i < totalParts + 1; i++) {
			BigInteger part = calculatePolynomial(coefficients, i, prime);
			parts[i - 1] = new SSSSecretPart(i, prime.bitLength(), part);
		}
		
		return parts;
	}

	private BigInteger gcd(BigInteger a, BigInteger b) {
		BigInteger x = BigInteger.ZERO;
		BigInteger last_x = BigInteger.ONE;
		
		while (b.compareTo(BigInteger.ZERO) != 0) {
			BigInteger quot = a.divide(b);
			BigInteger temp = b;
			b = a.mod(b);
			a = temp;
			BigInteger temp2 = x;
			x = last_x.subtract(quot.multiply(x));
			last_x = temp2;
		}
		
		return last_x;
	}
	
	private BigInteger divmod(BigInteger num, int den, BigInteger prime) {
		BigInteger gcd = gcd(BigInteger.valueOf(den), prime);
		BigInteger result = num.multiply(gcd);
		
		return result;
	}
	
	public BigInteger combine(SecretPart[] shares, BigInteger prime) {
		BigInteger result = BigInteger.ZERO;
		
		List<Integer> numeratorProducts = new ArrayList<Integer>();
		List<Integer> denominatorProducts = new ArrayList<Integer>();
		
		for (int i = 0; i < shares.length; i++) {
			List<Integer> numerators = new ArrayList<Integer>();
			List<Integer> denominators = new ArrayList<Integer>();
			
			for (int j = 0; j < shares.length; j++) {
				if (i == j) {
					continue;
				}
				
				numerators.add(0 - shares[j].getOrder());
				denominators.add(shares[i].getOrder() - shares[j].getOrder());
			}
			
			int numeratorProduct = Utilities.product(numerators);
			numeratorProducts.add(numeratorProduct);
			
			int denominatorProduct = Utilities.product(denominators);
			denominatorProducts.add(denominatorProduct);
		}
		
		int den = Utilities.product(denominatorProducts);
		
		List<BigInteger> tempValues = new ArrayList<BigInteger>();
		
		for (int i = 0; i < shares.length; i++) {
			BigInteger temp = shares[i].getPart().multiply(BigInteger.valueOf(numeratorProducts.get(i))).multiply(BigInteger.valueOf(den));
			
			if (temp.compareTo(BigInteger.ZERO) == 1) {
				temp = temp.subtract(prime);
			} else if (temp.compareTo(BigInteger.ZERO) == -1) {
				temp = temp.add(prime);
			}
			
			BigInteger temp2 = divmod(temp, denominatorProducts.get(i), prime);
			tempValues.add(temp2);
		}
		
		BigInteger temp3 = divmod(Utilities.sum(tempValues), den, prime);
		result = temp3.add(prime).mod(prime);
		
		return result;
	}
}
