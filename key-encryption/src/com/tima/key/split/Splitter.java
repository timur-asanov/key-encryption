package com.tima.key.split;

import java.math.BigInteger;

public interface Splitter {

	public SecretPart[] split(BigInteger secret, int numKeys, int totalParts, BigInteger prime);
	public BigInteger combine(SecretPart[] shares, BigInteger prime);
}
