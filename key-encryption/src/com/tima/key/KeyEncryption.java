package com.tima.key;

import java.math.BigInteger;
import java.util.Scanner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tima.key.encrypt.Encryptor;
import com.tima.key.encrypt.JasyptBCEncryptor;
import com.tima.key.split.SSSSecretPart;
import com.tima.key.split.SSSSplitter;
import com.tima.key.split.SecretPart;
import com.tima.key.split.Splitter;
import com.tima.key.util.HelperMessages;
import com.tima.key.util.InputReader;
import com.tima.key.util.Utilities;
import com.tima.key.util.ValidationException;

public class KeyEncryption {
	
	private static final Logger logger = LoggerFactory.getLogger(KeyEncryption.class);

	private static final String OPTION_KEY_HELP = "h";
	private static final String OPTION_KEY_VERBOSE = "v";
	private static final String OPTION_KEY_ENCRYPT = "e";
	private static final String OPTION_KEY_DONT_SPLIT = "ds";
	private static final String OPTION_KEY_DECRYPT = "d";
	private static final String OPTION_KEY_WITHOUT_SPLIT = "ws";
	private static final String OPTION_KEY_SPLIT = "s";
	private static final String OPTION_KEY_COMBINE = "c";
	private static final String OPTION_KEY_KEYS = "k";
	private static final String OPTION_KEY_TOTAL_PARTS = "p";
	
	private static Options buildCLIOptions() {
		Options options = new Options();
		
		Option help = new Option(OPTION_KEY_HELP, "help", false, "Specifying this option will print this message.");
		Option verbose = new Option(OPTION_KEY_VERBOSE, "verbose", false, "If provided, the program will output more information while running.");
		Option encrypt = new Option(OPTION_KEY_ENCRYPT, "encrypt", false, "If provided, the program will encrypt and split a key for secret sharing.");
		Option encryptDontSplit = new Option(OPTION_KEY_DONT_SPLIT, "dont_split", false, "If provided, the program will encrypt a key but will not split the encrypted key for secret sharing.");
		Option decrypt = new Option(OPTION_KEY_DECRYPT, "decrypt", false, "If provided, the program will decrypt and combine secret parts to get the key.");
		Option decryptWithoutSplit = new Option(OPTION_KEY_WITHOUT_SPLIT, "without_split", false, "If provided, the program will only decrypt the provided key.");
		Option split = new Option(OPTION_KEY_SPLIT, "split", false, "If provided, the program will split a key into parts for secret sharing without doing any encryption.");
		Option combine = new Option(OPTION_KEY_COMBINE, "combine", false, "If provided, the proram will combine secret parts into a key but will not do any decryption of the combined key.");
		Option splitKeys = new Option(OPTION_KEY_KEYS, "keys", true, "Number of parts needed to reconstruct the key.");
		Option splitParts = new Option(OPTION_KEY_TOTAL_PARTS, "parts", true, "Total number of parts that the key will be split into.");
		
		options.addOption(help);
		options.addOption(verbose);
		options.addOption(encrypt);
		options.addOption(encryptDontSplit);
		options.addOption(decrypt);
		options.addOption(decryptWithoutSplit);
		options.addOption(split);
		options.addOption(combine);
		options.addOption(splitKeys);
		options.addOption(splitParts);
		
		return options;
	}
	
	/**
	 * Encrypts and splits a key.
	 * 
	 * @param pwdArray - password for encryption
	 * @param key - key to encrypt and split
	 * @param numKeys - number of parts needed to reconstruct the key
	 * @param totalParts - total number of parts to split the key into
	 * 
	 * @return An array of secret parts of the encrypted key.
	 */
	protected static SecretPart[] encrypt(char[] pwdArray, String key, int numKeys, int totalParts) {
		SecretPart[] result = null;
		
		// encrypt the key
		String encryptedKey = encrypt(pwdArray, key);
		
		// split the key
		result = split(encryptedKey, numKeys, totalParts);
		
		return result;
	}

	/**
	 * Encrypts a key.
	 * 
	 * @param pwdArray - password for encryption
	 * @param key - key to encrypt and split
	 * 
	 * @return Encrypted key.
	 */
	protected static String encrypt(char[] pwdArray, String key) {
		String result = null;
		
		Encryptor encryptor = new JasyptBCEncryptor();
		result = encryptor.encrypt(pwdArray, key);
		
		logger.info(result);
		
		return result;
	}

	/**
	 * Combines and decrypts secret parts into a key.
	 * 
	 * @param pwdArray - password for decryption
	 * @param parts - secret parts to combine
	 * 
	 * @return Combined and decrypted key.
	 */
	protected static String decrypt(char[] pwdArray, SecretPart[] parts) {
		String result = null;
		
		// combine the key
		String combinedKey = combine(parts);
		
		// decrypt the key
		result = decrypt(pwdArray, combinedKey);
		
		return result;
	}
	
	/**
	 * Decrypts a key.
	 * 
	 * @param pwdArray - password for decryption
	 * @param key - the encrypted key to decrypt
	 * 
	 * @return Decrypted key.
	 */
	protected static String decrypt(char[] pwdArray, String key) {
		String result = null;
		
		Encryptor encryptor = new JasyptBCEncryptor();
		result = encryptor.decrypt(pwdArray, key);
		
		logger.info(result);
		
		return result;
	}
	
	/**
	 * Splits a key into secret parts.
	 * 
	 * @param key - the key to split
	 * @param numKeys - number of parts needed to reconstruct the key
	 * @param totalParts - total number of parts to split the key into
	 * 
	 * @return An array of secret parts of the key.
	 */
	protected static SecretPart[] split(String key, int numKeys, int totalParts) {
		SecretPart[] result = null;
		
		// convert the key into a number
		BigInteger encryptedNumber = new BigInteger(key.getBytes());
		
		// split the key into secret parts
		Splitter splitter = new SSSSplitter();
		result = splitter.split(encryptedNumber, numKeys, totalParts, Utilities.generatePrime(key.length() * 8 + 1));
		
		for (int i = 0; i < result.length; i++) {
			logger.info(result[i].toString());
		}
		
		return result;
	}
	
	/**
	 * Combines secret parts into a key.
	 * 
	 * @param parts - secret parts to combine
	 * 
	 * @return Combined key.
	 */
	protected static String combine(SecretPart[] parts) {
		Splitter splitter = new SSSSplitter();
		BigInteger combinedNumber = splitter.combine(parts, Utilities.generatePrime(((SSSSecretPart) parts[0]).getPrimeBitLength()));
		String combinedKey = new String(combinedNumber.toByteArray());
		
		logger.info(combinedKey);
		
		return combinedKey;
	}
	
	public static void main(String[] args) {
		boolean hideErrors = true;

		try {
			Options options = buildCLIOptions();

			CommandLineParser cliParser = new DefaultParser();
			CommandLine cli = cliParser.parse(options, args);

			// set the logger to output more statements if the verbose flag was provided
			if (cli.hasOption(OPTION_KEY_VERBOSE) == true) {
				logger.trace("verbose flag was provided");
				Configurator.setLevel(System.getProperty("log4j.logger"), Level.TRACE);
				hideErrors = false;
			}

			if (cli.hasOption(OPTION_KEY_HELP) == true) {
				HelperMessages.printHelpMessage();
			} else {
				int majorOptionCount = 0;

				majorOptionCount += cli.hasOption(OPTION_KEY_ENCRYPT) == true ? 1 : 0;
				majorOptionCount += cli.hasOption(OPTION_KEY_DECRYPT) == true ? 1 : 0;
				majorOptionCount += cli.hasOption(OPTION_KEY_SPLIT) == true ? 1 : 0;
				majorOptionCount += cli.hasOption(OPTION_KEY_COMBINE) == true ? 1 : 0;

				// only one of Encrypt, Decrypt, Split or Combine options is allowed
				if (majorOptionCount != 1) {
					logger.trace("Did not provide the correct number of major options. Provided [" + majorOptionCount + "] major options.");
					HelperMessages.printHelpMessage();
					System.exit(0);
				}

				try (Scanner scan = new Scanner(System.in)) {
					if (cli.hasOption(OPTION_KEY_ENCRYPT) == true) {
						if (cli.hasOption(OPTION_KEY_DONT_SPLIT) == true) {
							logger.trace("running encryption");

							String key = InputReader.inputKey(scan);
							char[] pwdArray = InputReader.inputPassword(scan);

							encrypt(pwdArray, key);
						} else {
							if (cli.hasOption(OPTION_KEY_KEYS) == true && cli.hasOption(OPTION_KEY_TOTAL_PARTS) == true) {
								logger.trace("running encryption and secret sharing");

								int numKeys = InputReader.readInt(cli.getOptionValue(OPTION_KEY_KEYS));
								int totalParts = InputReader.readInt(cli.getOptionValue(OPTION_KEY_TOTAL_PARTS));

								Utilities.validateForwards(numKeys, totalParts);

								String key = InputReader.inputKey(scan);
								char[] pwdArray = InputReader.inputPassword(scan);

								encrypt(pwdArray, key, numKeys, totalParts);
							} else {
								logger.trace("specified -e- flag but did not provide -ds- flag, or -k- and -p- values");
								HelperMessages.printEncryptionHelpMessage();
							}
						}
					} else if (cli.hasOption(OPTION_KEY_DECRYPT) == true) {
						if (cli.hasOption(OPTION_KEY_WITHOUT_SPLIT) == true) {
							logger.trace("running decryption");

							String key = InputReader.inputKey(scan);
							char[] pwdArray = InputReader.inputPassword(scan);

							decrypt(pwdArray, key);
						} else {
							if (cli.hasOption(OPTION_KEY_KEYS) == true) {
								logger.trace("running decryption and secret parts combining");

								int numKeys = InputReader.readInt(cli.getOptionValue(OPTION_KEY_KEYS));

								Utilities.validateNumKeys(numKeys);

								SecretPart[] parts = InputReader.inputParts(scan, numKeys);

								Utilities.validateBackwards(numKeys, parts);

								char[] pwdArray = InputReader.inputPassword(scan);

								Utilities.validateBackwards(numKeys, parts);

								decrypt(pwdArray, parts);
							} else {
								logger.trace("specified -d- flag but did not provide the -ws- flag or a -k- value");
								HelperMessages.printDecryptionHelpMessage();
							}
						}
					} else if (cli.hasOption(OPTION_KEY_SPLIT) == true) {
						if (cli.hasOption(OPTION_KEY_KEYS) == true && cli.hasOption(OPTION_KEY_TOTAL_PARTS) == true) {
							logger.trace("running secret sharing");

							int numKeys = InputReader.readInt(cli.getOptionValue(OPTION_KEY_KEYS));
							int totalParts = InputReader.readInt(cli.getOptionValue(OPTION_KEY_TOTAL_PARTS));

							Utilities.validateForwards(numKeys, totalParts);

							String key = InputReader.inputKey(scan);

							split(key, numKeys, totalParts);
						} else {
							logger.trace("specified -s- flag but did not provide -k- and / or -p- values");
							HelperMessages.printSplitHelpMessage();
						}
					} else if (cli.hasOption(OPTION_KEY_COMBINE) == true) {
						if (cli.hasOption(OPTION_KEY_KEYS) == true) {
							logger.trace("running secret parts combining");

							int numKeys = InputReader.readInt(cli.getOptionValue(OPTION_KEY_KEYS));

							Utilities.validateNumKeys(numKeys);

							SecretPart[] parts = InputReader.inputParts(scan, numKeys);

							Utilities.validateBackwards(numKeys, parts);

							combine(parts);
						} else {
							logger.trace("specified -c- flag but did not provide a -k- value");
							HelperMessages.printCombineHelpMessage();
						}
					} else {
						logger.trace("provided options did not match any existing options");
						HelperMessages.printHelpMessage();
					}
				}
			}
		} catch (UnrecognizedOptionException uoe) {
			logger.trace("provided an unrecognized option [" + uoe.getOption() + "]");
			HelperMessages.printHelpMessage();
		} catch (ValidationException ve) {
			logger.error(ve.getMessage());
		} catch (EncryptionOperationNotPossibleException eonpe) {
			logger.error("Was not able to encrypt / decrypt the key.");
		} catch (Exception e) {
			if (hideErrors == true) {
				logger.error("An error has occured. Please re-try.");
			} else {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
