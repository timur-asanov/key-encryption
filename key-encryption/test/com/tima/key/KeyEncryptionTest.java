package com.tima.key;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tima.key.split.SSSSecretPart;
import com.tima.key.split.SecretPart;

public class KeyEncryptionTest {

	private static final Logger logger = LoggerFactory.getLogger(KeyEncryptionTest.class);
	
	private static List<TestDataHolder> dataDecryptOnly = null;
	private static List<TestDataHolder> dataDecryptAndCombine = null;
	private static List<TestDataHolder> dataEcryptOnly = null;
	private static List<TestDataHolder> dataEcryptAndSplit = null;
	private static List<TestDataHolder> dataSplitOnly = null;
	private static List<TestDataHolder> dataCombineOnly = null;
	
	@BeforeClass
	public static void initData() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			mapper.setSerializationInclusion(Include.NON_NULL);
			mapper.setSerializationInclusion(Include.NON_DEFAULT);
			
			File decryptOnlyFile = new File(KeyEncryptionTest.class.getClassLoader().getResource("data_decrypt_only.json").getFile());
			String decryptOnlyJSON = new String(Files.readAllBytes(Paths.get(decryptOnlyFile.getAbsolutePath())));
			dataDecryptOnly = mapper.readValue(decryptOnlyJSON, new TypeReference<List<TestDataHolder>>(){});
			
			File encryptOnlyFile = new File(KeyEncryptionTest.class.getClassLoader().getResource("data_encrypt_only.json").getFile());
			String encryptOnlyJSON = new String(Files.readAllBytes(Paths.get(encryptOnlyFile.getAbsolutePath())));
			dataEcryptOnly = mapper.readValue(encryptOnlyJSON, new TypeReference<List<TestDataHolder>>(){});
			
			File decryptAndCombineFile = new File(KeyEncryptionTest.class.getClassLoader().getResource("data_decrypt_and_combine.json").getFile());
			String decryptAndCombineJSON = new String(Files.readAllBytes(Paths.get(decryptAndCombineFile.getAbsolutePath())));
			dataDecryptAndCombine = mapper.readValue(decryptAndCombineJSON, new TypeReference<List<TestDataHolder>>(){});
			
			for (TestDataHolder testDataHolder : dataDecryptAndCombine) {
				testDataHolder.setParts(TestingUtilities.getRandomNumberOfItemsFromList(testDataHolder.getParts(), testDataHolder.getNumKeys()));
			}
			
			File encryptAndSplitFile = new File(KeyEncryptionTest.class.getClassLoader().getResource("data_encrypt_and_split.json").getFile());
			String encryptAndSplitJSON = new String(Files.readAllBytes(Paths.get(encryptAndSplitFile.getAbsolutePath())));
			dataEcryptAndSplit = mapper.readValue(encryptAndSplitJSON, new TypeReference<List<TestDataHolder>>(){});
			
			File combineOnlyFile = new File(KeyEncryptionTest.class.getClassLoader().getResource("data_combine_only.json").getFile());
			String combineOnlyJSON = new String(Files.readAllBytes(Paths.get(combineOnlyFile.getAbsolutePath())));
			dataCombineOnly = mapper.readValue(combineOnlyJSON, new TypeReference<List<TestDataHolder>>(){});
			
			for (TestDataHolder testDataHolder : dataCombineOnly) {
				testDataHolder.setParts(TestingUtilities.getRandomNumberOfItemsFromList(testDataHolder.getParts(), testDataHolder.getNumKeys()));
			}
			
			File splitOnlyFile = new File(KeyEncryptionTest.class.getClassLoader().getResource("data_split_only.json").getFile());
			String splitOnlyJSON = new String(Files.readAllBytes(Paths.get(splitOnlyFile.getAbsolutePath())));
			dataSplitOnly = mapper.readValue(splitOnlyJSON, new TypeReference<List<TestDataHolder>>(){});
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testDecrypt() {
		try {
			for (TestDataHolder testDataHolder : dataDecryptOnly) {
				String result = KeyEncryption.decrypt(testDataHolder.getPwdArray(), testDataHolder.getEncryptedKey());
				
				Assert.assertNotNull(result);
				Assert.assertEquals(testDataHolder.getKey(), result);
			}
			
			for (TestDataHolder testDataHolder : dataDecryptAndCombine) {
				String result = KeyEncryption.decrypt(testDataHolder.getPwdArray(), testDataHolder.getParts());

				Assert.assertNotNull(result);
				Assert.assertEquals(testDataHolder.getKey(), result);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testEncrypt() {
		try {
			for (TestDataHolder testDataHolder : dataEcryptOnly) {
				String encryptedKey = KeyEncryption.encrypt(testDataHolder.getPwdArray(), testDataHolder.getKey());
				
				Assert.assertNotNull(encryptedKey);
				
				String decryptedKey = KeyEncryption.decrypt(testDataHolder.getPwdArray(), encryptedKey);
				
				Assert.assertNotNull(decryptedKey);
				Assert.assertEquals(testDataHolder.getKey(), decryptedKey);
			}
			
			for (TestDataHolder testDataHolder : dataEcryptAndSplit) {
				SecretPart[] secretParts = KeyEncryption.encrypt(testDataHolder.getPwdArray(), testDataHolder.getKey(), testDataHolder.getNumKeys(), testDataHolder.getTotalParts());
				
				Assert.assertNotNull(secretParts);
				Assert.assertEquals(secretParts.length, testDataHolder.getTotalParts());
				
				SSSSecretPart[] secretPartsToCombine = TestingUtilities.getRandomNumberOfItemsFromList(Arrays.asList((SSSSecretPart[]) secretParts), testDataHolder.getNumKeys());
				
				Assert.assertNotNull(secretPartsToCombine);
				Assert.assertEquals(secretPartsToCombine.length, testDataHolder.getNumKeys());
				
				String result = KeyEncryption.decrypt(testDataHolder.getPwdArray(), secretPartsToCombine);
				
				Assert.assertNotNull(result);
				Assert.assertEquals(testDataHolder.getKey(), result);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testSplit() {
		try {
			for (TestDataHolder testDataHolder : dataSplitOnly) {
				SecretPart[] secretParts = KeyEncryption.split(testDataHolder.getKey(), testDataHolder.getNumKeys(), testDataHolder.getTotalParts());
				
				Assert.assertNotNull(secretParts);
				Assert.assertEquals(secretParts.length, testDataHolder.getTotalParts());
				
				String result = KeyEncryption.combine(secretParts);
				
				Assert.assertNotNull(result);
				Assert.assertEquals(result, testDataHolder.getKey());
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testCombine() {
		try {
			for (TestDataHolder testDataHolder : dataCombineOnly) {
				String result = KeyEncryption.combine(testDataHolder.getParts());
				
				Assert.assertNotNull(result);
				Assert.assertEquals(result, testDataHolder.getKey());
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
