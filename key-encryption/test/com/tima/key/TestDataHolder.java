package com.tima.key;

import com.tima.key.split.SSSSecretPart;

public class TestDataHolder {
	private String key = null;
	private String encryptedKey = null;
	private char[] pwdArray = null;
	private SSSSecretPart[] parts = null;
	private int numKeys;
	private int totalParts;
	
	public TestDataHolder() {}

	public String getKey() {
		return key;
	}

	public String getEncryptedKey() {
		return encryptedKey;
	}

	public char[] getPwdArray() {
		return pwdArray;
	}

	public SSSSecretPart[] getParts() {
		return parts;
	}

	public void setParts(SSSSecretPart[] parts) {
		this.parts = parts;
	}

	public int getNumKeys() {
		return numKeys;
	}

	public int getTotalParts() {
		return totalParts;
	}
}
