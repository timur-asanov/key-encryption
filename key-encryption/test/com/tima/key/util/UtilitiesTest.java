package com.tima.key.util;

import static org.junit.Assert.fail;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tima.key.TestDataHolder;
import com.tima.key.TestingUtilities;

public class UtilitiesTest {

	private static final Logger logger = LoggerFactory.getLogger(UtilitiesTest.class);
	
	@BeforeClass
	public static void initData() {
		try {
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testSplitStringIntoPart() {
		try {
			String[] invalidParts = new String[] {
					"3pwd1612272360493835042361654651861350117081323402585411945773652308196353326810361",
					"3-pwd-1612272360493835042361654651861350117081323402585411945773652308196353326810361",
					"p-705-1612272360493835042361654651861350117081323402585411945773652308196353326810361",
					"1-705-test",
					"1-705-",
					"2--1612272360493835042361654651861350117081323402585411945773652308196353326810361",
					"-705-1612272360493835042361654651861350117081323402585411945773652308196353326810361",
					"1--",
					"--1612272360493835042361654651861350117081323402585411945773652308196353326810361",
					"-pwd-",
					"1-705-     ",
					"1-    -1612272360493835042361654651861350117081323402585411945773652308196353326810361",
					" -705-1612272360493835042361654651861350117081323402585411945773652308196353326810361",
					"4-705-16122@@@7236####04938$$$35042361654651861350117081323402585411945773652308196353326810361",
					"4-$$#-1612272360493835042361654651861350117081323402585411945773652308196353326810361",
					"^-705-1612272360493835042361654651861350117081323402585411945773652308196353326810361",
					"1-7^%-1612272360493835042361654651861350117081323402585411945773!@"
			};
			
			String[] validParts = new String[] {
					"1-273-6866627235947964346197978261017396648162109221415293921580530285899169934422198468",
					"1-1-1",
					"123-27354-00000",
					"0-000-0000000000000000000",
					"6-705-17819752143501027398153414809719445144473330334774826299057838711458329234117439003714042843923125047587713636559545925640573585109169596163074812924550479361457842677572223079741502801049019571968005015253427614"
			};
			
			for (String part : invalidParts) {
				try {
					Utilities.splitStringIntoPart(part);
					fail("Expected an exception but did not get one.");
				} catch (Exception e) {}
			}
			
			for (String part : validParts) {
				Utilities.splitStringIntoPart(part);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testValidateKey() {
		try {
			String[] invalidKeys = new String[] {"", "   ", null, "\n", "\t"};
			String[] validKeys = new String[] {" key1  ", "key2", "1234", "1", "p", "!@#$%^&*()_+{}[]:;<,>.?/'\\\""};
			
			for (String key : invalidKeys) {
				try {
					Utilities.validateKey(key);
					fail("Expected an exception but did not get one.");
				} catch (Exception e) {}
			}

			for (String key : validKeys) {
				Utilities.validateKey(key);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testValidatePassword() {
		try {
			List<char[]> invalidPasswords = Arrays.asList("".toCharArray(), " ".toCharArray(), null);
			List<char[]> validPasswords = Arrays.asList("1".toCharArray(), "@".toCharArray(), "   pwd1   ".toCharArray());
			
			for (char[] password : invalidPasswords) {
				try {
					Utilities.validatePassword(password);
					fail("Expected an exception but did not get one.");
				} catch (Exception e) {}
			}
			
			for (char[] password : validPasswords) {
				Utilities.validatePassword(password);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testValidateForwards() {
		try {
			List<int[]> invalidForwardValues = Arrays.asList(
					new int[] {1, 3}, 
					new int[] {1, 2}, 
					new int[] {0, 3}, 
					new int[] {0, 0}, 
					new int[] {1, 11}, 
					new int[] {4, 20}, 
					new int[] {5, 22555}, 
					new int[] {50, 4}, 
					new int[] {5, 3}, 
					new int[] {2, 0}
			);
			
			List<int[]> validForwardValues = Arrays.asList(
					new int[] {2, 3},
					new int[] {3, 3},
					new int[] {3, 4},
					new int[] {2, 10},
					new int[] {2, 8},
					new int[] {5, 6},
					new int[] {9, 9}
			);
			
			for (int[] item : invalidForwardValues) {
				try {
					Utilities.validateForwards(item[0], item[1]);
					fail("Expected an exception but did not get one.");
				} catch (Exception e) {}
			}
			
			for (int[] item : validForwardValues) {
				Utilities.validateForwards(item[0], item[1]);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@Test
	public void testValidateBackwards() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			mapper.setSerializationInclusion(Include.NON_NULL);
			mapper.setSerializationInclusion(Include.NON_DEFAULT);
			
			File invalidPartsFile = new File(UtilitiesTest.class.getClassLoader().getResource("data_invalid_parts.json").getFile());
			String invalidPartsJSON = new String(Files.readAllBytes(Paths.get(invalidPartsFile.getAbsolutePath())));
			List<TestDataHolder> invalidParts = mapper.readValue(invalidPartsJSON, new TypeReference<List<TestDataHolder>>(){});
			
			for (TestDataHolder testDataHolder : invalidParts) {
				testDataHolder.setParts(TestingUtilities.getRandomNumberOfItemsFromList(testDataHolder.getParts(), testDataHolder.getNumKeys()));
				
				try {
					Utilities.validateBackwards(testDataHolder.getNumKeys(), testDataHolder.getParts());
					fail("Expected an exception but did not get one.");
				} catch (Exception e) {}
			}
			
			File validPartsFile = new File(UtilitiesTest.class.getClassLoader().getResource("data_valid_parts.json").getFile());
			String validPartsJSON = new String(Files.readAllBytes(Paths.get(validPartsFile.getAbsolutePath())));
			List<TestDataHolder> validParts = mapper.readValue(validPartsJSON, new TypeReference<List<TestDataHolder>>(){});
			
			for (TestDataHolder testDataHolder : validParts) {
				testDataHolder.setParts(TestingUtilities.getRandomNumberOfItemsFromList(testDataHolder.getParts(), testDataHolder.getNumKeys()));
				
				Utilities.validateBackwards(testDataHolder.getNumKeys(), testDataHolder.getParts());
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
