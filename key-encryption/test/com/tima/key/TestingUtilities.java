package com.tima.key;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.tima.key.split.SSSSecretPart;

public class TestingUtilities {

	public static SSSSecretPart[] getRandomNumberOfItemsFromList(List<SSSSecretPart> allItems, int numToGet) {
		SSSSecretPart[] result = new SSSSecretPart[numToGet];
		Collections.shuffle(allItems);
		
		for (int i = 0; i < numToGet; i++) {
			result[i] = allItems.get(i);
		}
		
		return result;
	}
	
	public static SSSSecretPart[] getRandomNumberOfItemsFromList(SSSSecretPart[] allItems, int numToGet) {
		return getRandomNumberOfItemsFromList(Arrays.asList(allItems), numToGet);
	}
}
