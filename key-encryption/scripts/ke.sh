#!/bin/sh

ke_jar="key-encryption.jar"
ke_lib="lib"
java_loc="java"

chmod +x $ke_jar
$java_loc -cp $ke_jar:$ke_lib/* com.tima.key.KeyEncryption "$@"

